Application start -  
Spring boot scan classpath, creates beans, configure them with BeanPostProcessors
and put them in IoC container.

For configuration we need: 
* accessToken - required by VK when receives message from server.  
* secret - need for the application when get request from VK
* confirmation - need for response to confirmation request 
* vkApiVersion - 5.122
___

[getJavaJob](http://www.getjavajob.com/)
 
