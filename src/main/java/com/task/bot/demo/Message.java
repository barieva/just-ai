package com.task.bot.demo;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Message {
    private String type;
    @JsonAlias("group_id")
    private String groupId;
    private String secret;
    @JsonAlias("object")
    private MessageContent messageContent;
}