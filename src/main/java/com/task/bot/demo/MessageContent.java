package com.task.bot.demo;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MessageContent {
    @JsonAlias("user_id")
    private String userId;
    private String body;
}