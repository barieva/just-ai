package com.task.bot.demo;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.net.URISyntaxException;

import static com.task.bot.demo.Constants.CONFIRMATION;
import static com.task.bot.demo.Constants.MESSAGE_NEW;

@Controller
@Slf4j
@Getter
@Setter
public class MessageController {
    @Value("${secret}")
    private String secretKey;
    @Value("${confirmation}")
    private String confirmation;
    @Value("${accessToken}")
    private String accessToken;
    @Value("${vkApiVersion}")
    private String vkApiVersion;
    private MessageService messageService;

    @Autowired
    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping("/bot")
    public ModelAndView getMessage(@RequestBody Message message) {
        ModelAndView modelAndView = new ModelAndView();
        if (secretKey.equals(message.getSecret())) {
            if (CONFIRMATION.equals(message.getType())) {
                modelAndView.setViewName("forward:/confirmation");
                return modelAndView;
            } else if (isNewTextMessage(message)) {
                log.info("Take new message: " + message);
                modelAndView.addObject("message", message);
                modelAndView.setViewName("forward:/sendMessage");
                return modelAndView;
            }
        }
        modelAndView.setViewName("forward:/notSupported");
        return modelAndView;
    }

    private boolean isNewTextMessage(Message message) {
        return MESSAGE_NEW.equals(message.getType()) && !message.getMessageContent().getBody().isEmpty();
    }

    @RequestMapping("/confirmation")
    @ResponseBody
    public String sendConfirmationKey() {
        return confirmation;
    }

    @RequestMapping("/sendMessage")
    @ResponseBody
    public String sendMessage(@RequestAttribute("message") Message message) throws IOException, URISyntaxException, HttpException {
        ResponseMessage responseMessage = getResponseMessage(message);
        log.info("Message to send response: " + responseMessage);
        messageService.sendMessageToUser(responseMessage);
        return "ok";
    }

    private ResponseMessage getResponseMessage(@RequestAttribute("message") Message message) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setReceiverId(message.getMessageContent().getUserId());
        responseMessage.setText("Вы сказали: " + message.getMessageContent().getBody());
        responseMessage.setAccessToken(accessToken);
        responseMessage.setVkApiVersion(vkApiVersion);
        return responseMessage;
    }

    @RequestMapping("/notSupported")
    @ResponseBody
    public String notSupported() {
        return "Not supported";
    }
}