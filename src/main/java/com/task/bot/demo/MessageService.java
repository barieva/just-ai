package com.task.bot.demo;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.String.valueOf;

@Slf4j
@Service
public class MessageService {
    private AtomicInteger randomId = new AtomicInteger();

    public void sendMessageToUser(ResponseMessage responseMessage) throws URISyntaxException, HttpException {
        URI uri = getBuiltURI(responseMessage);
        sendRequest(uri);
    }

    private URI getBuiltURI(ResponseMessage responseMessage) throws URISyntaxException {
        URIBuilder builder = new URIBuilder("https://api.vk.com/method/messages.send");
        builder.setParameter("user_id", responseMessage.getReceiverId())
                .setParameter("message", responseMessage.getText())
                .setParameter("access_token", responseMessage.getAccessToken())
                .setParameter("v", responseMessage.getVkApiVersion())
                .setParameter("random_id", valueOf(randomId.incrementAndGet()));
        return builder.build();
    }

    private void sendRequest(URI uri) throws HttpException {
        HttpGet httpGet = new HttpGet(uri);
        try (CloseableHttpClient client = HttpClients.createDefault();) {
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            client.execute(httpGet);
            log.info("Message have sent by the uri: " + uri);
        } catch (IOException e) {
            throw new HttpException("Can`t send the request" + httpGet);
        }
    }
}