package com.task.bot.demo;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseMessage {
    @JsonAlias("user_id")
    private String receiverId;
    @JsonAlias("message")
    private String text;
    @JsonAlias("access_token")
    private String accessToken;
    @JsonAlias("v")
    private String vkApiVersion;

    @Override
    public String toString() {
        return "{\"user_id\"=\"" + receiverId + '\"' +
                ", \"message\"=\"" + text + '\"' +
                ", \"access_token\"=\"" + accessToken + '\"' +
                ", \"v\"=\"" + vkApiVersion + '\"' + '}';
    }
}