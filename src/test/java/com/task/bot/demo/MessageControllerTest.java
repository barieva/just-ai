package com.task.bot.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.servlet.ModelAndView;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(SpringExtension.class)
@ComponentScan(basePackageClasses = MessageController.class)
@SpringBootTest
class MessageControllerTest {
    @Mock
    MessageService messageService;
    @InjectMocks
    MessageController messageController;

    @BeforeEach
    public void setUp(){
        messageController.setAccessToken("8cc9c72cc6c035eb3257b8c2989466aa401f4fc8de89744de6276450976bbe6123786434068368ee941b7");
        messageController.setSecretKey("f12345F");
        messageController.setVkApiVersion("5.122");
        messageController.setConfirmation("774d1cb7");
    }

    @Test
    public void getConfirmationMessage() {
        Message message = new Message();
        message.setSecret("f12345F");
        message.setType("confirmation");
        ModelAndView actual = messageController.getMessage(message);
        assertEquals( "forward:/confirmation", actual.getViewName(),"Need to forward to confirmation");
    }

    @Test
    public void getMessageWithWrongSecretKey() {
        Message message = new Message();
        message.setSecret("WRONG");
        ModelAndView actual = messageController.getMessage(message);
        assertEquals(   "forward:/notSupported", actual.getViewName(),"Secret key is wrong then it is not supported");
    }

    @Test
    public void getNewTextMessage() {
        Message message = new Message();
        message.setSecret("f12345F");
        message.setType("message_new");
        MessageContent messageContent = new MessageContent();
        messageContent.setBody("some text");
        message.setMessageContent(messageContent);
        ModelAndView actual = messageController.getMessage(message);
        assertEquals( "forward:/sendMessage", actual.getViewName(),"There is new text message and need forward to /sendMessage");
    }

    @Test
    public void getNewMessageWithNoText() {
        Message message = new Message();
        message.setSecret("f12345F");
        message.setType("message_new");
        MessageContent messageContent = new MessageContent();
        messageContent.setBody("");
        message.setMessageContent(messageContent);
        ModelAndView actual = messageController.getMessage(message);
        assertEquals( "forward:/notSupported", actual.getViewName(),"There is no text message and need forward to /notSupported");
    }

    @Test
    public void getMessageWithoutMessageContent() {
        Message message = new Message();
        message.setSecret("f12345F");
        message.setType("message_new");
        assertThrows(NullPointerException.class, () -> messageController.getMessage(message));
    }

    @Test
    public void getNotSupportedTypeMessage() {
        Message message = new Message();
        message.setSecret("f12345F");
        message.setType("other_type");
        ModelAndView actual = messageController.getMessage(message);
        assertEquals( "forward:/notSupported", actual.getViewName(),"This type not supported and need to forward to /notSupported");
    }

    @Test
    public void sendConfirmationTest() {
        assertEquals("774d1cb7", messageController.sendConfirmationKey(),"Confirmation key does not match");
    }

    @Test
    public void getNotSupportedMessage() {
        assertEquals("Not supported", messageController.notSupported());
    }

    @Test
    public void sendMessage() throws Exception {
        Message message = new Message();
        message.setSecret("f12345F");
        message.setType("other_type");
        MessageContent messageContent = new MessageContent();
        messageContent.setBody("Text");
        message.setMessageContent(messageContent);
        assertEquals("ok", messageController.sendMessage(message));
    }

    @Test
    public void sendEmptyMessage() {
        Message message = new Message();
        assertThrows(NullPointerException.class, () -> messageController.sendMessage(message));
    }

    @Test
    public void sendNullMessage() {
        assertThrows(NullPointerException.class, () -> messageController.sendMessage(null));
    }
}