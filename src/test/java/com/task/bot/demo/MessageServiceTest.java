package com.task.bot.demo;

import org.apache.http.HttpException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class MessageServiceTest {
    @Autowired
    MessageService messageService;


    @Test
    public void sendMessageToUser() throws URISyntaxException, HttpException {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setVkApiVersion("1");
        responseMessage.setAccessToken("accessToken");
        responseMessage.setReceiverId("id");
        responseMessage.setText("Text");
        messageService.sendMessageToUser(responseMessage);
    }

    @Test
    public void sendEmptyMessageToUser() throws URISyntaxException, HttpException {
        ResponseMessage responseMessage = new ResponseMessage();
        messageService.sendMessageToUser(responseMessage);
    }

    @Test
    public void sendNullMessageToUser()  {
        assertThrows(NullPointerException.class,()-> messageService.sendMessageToUser(null));
    }
}